import { ref } from 'vue'

export const runCode = (defaultCode = '') => {
  const codeNow = ref(defaultCode)

  const setCode = (code) => {
    codeNow.value = code
  }

  return { codeNow, setCode }
}
