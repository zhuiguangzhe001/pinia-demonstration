import { ref } from 'vue'
import { useLoadingBar } from 'naive-ui'

export const loading = () => {
  const load = ref(true)
  const loadingBar = useLoadingBar()
  loadingBar.start()
  setTimeout(() => {
    loadingBar.finish()
    load.value = false
  }, 300)
  return load
}